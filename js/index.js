$(document).ready(function(){
	$('.nav_body img').on('click', function(){
		alert('This is logo!');
	});
	$('.logout').on('click', function(){
		alert('You are gonna logout!');
	});
	$('.service').on('click', function(){
		alert('You clicked User Icon!');
	});
	$('.settings').on('click', function(){
		alert('You clicked Settings Icon');
	});
	$('.help').on('click', function(){
		alert('You clicked Help Icon');
	});

	google.charts.load('current', {'packages':['corechart','bar', 'gantt']});
	google.charts.setOnLoadCallback(drawVisualization);

	function drawVisualization() {
	// Some raw data (not necessarily accurate)
		var combo_chart_data = google.visualization.arrayToDataTable([
			 ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
			 ['2004/05',  165,      938,         522,             998,           450,      614.6],
			 ['2005/06',  135,      1120,        599,             1268,          288,      682],
			 ['2006/07',  157,      1167,        587,             807,           397,      623],
			 ['2007/08',  139,      1110,        615,             968,           215,      609.4],
			 ['2008/09',  136,      691,         629,             1026,          366,      569.6]
		]);

		var combo_chart_options = {
			legend: {position: 'none'},
			seriesType: 'bars',
			series: {5: {type: 'line'}}
		};

		var combo_chart = new google.visualization.ComboChart(document.getElementById('combo_chart'));
		combo_chart.draw(combo_chart_data, combo_chart_options);
		//////////////////

		var bar_chart_data = google.visualization.arrayToDataTable([
			['Year', 'Sales', 'Expenses', 'Profit'],
			['2014', 1000, 400, 200],
			['2015', 1170, 460, 250],
			['2016', 660, 1120, 300],
			['2017', 1030, 540, 350]
		]);
		var bar_chart_options = {
			legend: {position: 'none'},
			bars: 'horizontal' // Required for Material Bar Charts.
		};

		var bar_chart = new google.charts.Bar(document.getElementById('bar_chart'));

		// bar_chart.draw(bar_chart_data, google.charts.Bar.convertOptions(bar_chart_options));
		bar_chart.draw(bar_chart_data, bar_chart_options);

		///////////////////////
		var gantt_chart_data = new google.visualization.DataTable();
		gantt_chart_data.addColumn('string', 'Task ID');
		gantt_chart_data.addColumn('string', 'Task Name');
		gantt_chart_data.addColumn('string', 'Resource');
		gantt_chart_data.addColumn('date', 'Start Date');
		gantt_chart_data.addColumn('date', 'End Date');
		gantt_chart_data.addColumn('number', 'Duration');
		gantt_chart_data.addColumn('number', 'Percent Complete');
		gantt_chart_data.addColumn('string', 'Dependencies');
		gantt_chart_data.addRows([
			['2014Spring', 'Spring 2014', 'spring',
			 new Date(2014, 2, 22), new Date(2014, 5, 20), null, 100, null],
			['2014Summer', 'Summer 2014', 'summer',
			 new Date(2014, 5, 21), new Date(2014, 8, 20), null, 100, null],
			['2014Autumn', 'Autumn 2014', 'autumn',
			 new Date(2014, 8, 21), new Date(2014, 11, 20), null, 100, null],
			['2014Winter', 'Winter 2014', 'winter',
			 new Date(2014, 11, 21), new Date(2015, 2, 21), null, 100, null],
			['2015Spring', 'Spring 2015', 'spring',
			 new Date(2015, 2, 22), new Date(2015, 5, 20), null, 50, null],
			['2015Summer', 'Summer 2015', 'summer',
			 new Date(2015, 5, 21), new Date(2015, 8, 20), null, 0, null],
			['2015Autumn', 'Autumn 2015', 'autumn',
			 new Date(2015, 8, 21), new Date(2015, 11, 20), null, 0, null],
			['2015Winter', 'Winter 2015', 'winter',
			 new Date(2015, 11, 21), new Date(2016, 2, 21), null, 0, null],
			['Football', 'Football Season', 'sports',
			 new Date(2014, 8, 4), new Date(2015, 1, 1), null, 100, null],
			['Baseball', 'Baseball Season', 'sports',
			 new Date(2015, 2, 31), new Date(2015, 9, 20), null, 14, null],
			['Basketball', 'Basketball Season', 'sports',
			 new Date(2014, 9, 28), new Date(2015, 5, 20), null, 86, null],
			['Hockey', 'Hockey Season', 'sports',
			 new Date(2014, 9, 8), new Date(2015, 5, 21), null, 89, null]
		]);
		var gantt_chart_options = {
			width: '100%',
			gantt: {
			  trackHeight: 24,
			  barHeight: 18,
			}
		};

		var gantt_chart = new google.visualization.Gantt(document.getElementById('gantt_chart'));
		gantt_chart.draw(gantt_chart_data, gantt_chart_options);

	}
	$(window).resize(function(){
		drawVisualization();
	})

});
$(document).on('shown.bs.tab', '.top a[data-toggle="tab"]', function (e) {
		$('.navbar-fixed-left .top a').removeClass('actived');
		$(this).addClass('actived');
})